package com.blueskykong.tm.core.service;

import com.blueskykong.tm.common.entity.TransactionMsg;

import java.util.List;

/**
 * @author keets
 * @data 2018/3/29.
 */
public interface ExternalNettyService {

    /**
     * pre-commit msgs
     *
     * @param preCommitMsgs
     */
    public Boolean preSend(List<TransactionMsg> preCommitMsgs);

    /**
     * confirm msgs
     *
     * @param success
     */
    public void postSend(Boolean success);

    /**
     * msgs after consuming
     *
     * @param msg
     * @param success
     */
    public void consumedSend(TransactionMsg msg, Boolean success);
}
