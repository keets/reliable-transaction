package com.blueskykong.tm.mq.consumer.spi;

import com.blueskykong.tm.common.entity.TransactionMsg;
import com.blueskykong.tm.mq.TxMQClient;

/**
 * @Author: JasonChen86899
 */
public interface TxMQConsumer extends TxMQClient {

    /**
     * 订阅消息
     */
    void subScription();

    /**
     * 消费消息
     *
     * @param mesage
     * @return
     */
    boolean consume(TransactionMsg mesage);

}
