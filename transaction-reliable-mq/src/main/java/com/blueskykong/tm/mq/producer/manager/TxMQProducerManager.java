package com.blueskykong.tm.mq.producer.manager;

import com.blueskykong.tm.mq.TxMQManager;
import com.blueskykong.tm.mq.producer.spi.TxMQProducer;

/**
 * @Author: JasonChen86899
 */
public class TxMQProducerManager extends TxMQManager<TxMQProducer> {

    private static final TxMQProducerManager INSTANCE = new TxMQProducerManager();

    private TxMQProducerManager() {
        loadInital(TxMQProducer.class);
    }

    public static TxMQProducerManager getInstance() {
        return INSTANCE;
    }
}
