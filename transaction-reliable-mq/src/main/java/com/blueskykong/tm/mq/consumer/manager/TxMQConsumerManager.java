package com.blueskykong.tm.mq.consumer.manager;

import com.blueskykong.tm.mq.TxMQManager;
import com.blueskykong.tm.mq.consumer.spi.TxMQConsumer;

/**
 * @Author: JasonChen86899
 */
public final class TxMQConsumerManager extends TxMQManager<TxMQConsumer> {

    private static final TxMQConsumerManager INSTANCE = new TxMQConsumerManager();

    private TxMQConsumerManager() {
        loadInital(TxMQConsumer.class);
    }

    public static TxMQConsumerManager getInstance() {
        return INSTANCE;
    }

}
