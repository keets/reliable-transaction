package com.blueskykong.tm.mq.producer.spi;

import com.blueskykong.tm.common.entity.TransactionMsg;
import com.blueskykong.tm.mq.TxMQClient;

/**
 * @Author: JasonChen86899
 */
public interface TxMQProducer extends TxMQClient {

    /**
     * 生产者生产事务数据
     *
     * @param data
     */
    boolean produce(TransactionMsg data);

}
