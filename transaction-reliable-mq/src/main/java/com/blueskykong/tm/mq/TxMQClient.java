package com.blueskykong.tm.mq;

import com.blueskykong.tm.mq.consumer.spi.TxMQConsumer;

/**
 * @Author: JasonChen86899
 */
public interface TxMQClient {

    /**
     * 返回 MQ 连接对象，用于返回mq生产者和消费者的客户端实例,
     * 实例化实现必须实现此方法并返回连接成功的客户端对象
     *
     * @return mq类型名称
     */
    TxMQConsumer getInstance();
}
