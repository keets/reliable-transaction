package com.blueskykong.tm.server.service.impl;

import com.blueskykong.tm.common.constant.CommonConstant;
import com.blueskykong.tm.common.entity.TransactionMsg;
import com.blueskykong.tm.common.enums.TransactionRoleEnum;
import com.blueskykong.tm.common.enums.TransactionStatusEnum;
import com.blueskykong.tm.common.holder.DateUtils;
import com.blueskykong.tm.common.netty.bean.TxTransactionGroup;
import com.blueskykong.tm.common.netty.bean.TxTransactionItem;
import com.blueskykong.tm.server.config.Constant;
import com.blueskykong.tm.server.service.TxManagerService;
import com.blueskykong.tm.server.stream.MsgSource;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@EnableBinding({MsgSource.class})
@Component
@SuppressWarnings("unchecked")
public class TxManagerServiceImpl implements TxManagerService {


    private final RedisTemplate redisTemplate;
    private final MsgSource msgSource;


    @Autowired
    public TxManagerServiceImpl(RedisTemplate redisTemplate, MsgSource msgSource) {
        this.redisTemplate = redisTemplate;
        this.msgSource = msgSource;
    }


    /**
     * 保存事务组 在事务发起方的时候进行调用
     *
     * @param txTransactionGroup 事务组
     * @return true 成功 false 失败
     */
    @Override
    public Boolean saveTxTransactionGroup(TxTransactionGroup txTransactionGroup) {
        try {
            final String groupId = txTransactionGroup.getId();
            //保存数据 到sortSet
            redisTemplate.opsForZSet()
                    .add(CommonConstant.REDIS_KEY_SET, groupId, CommonConstant.REDIS_SCOPE);

            final List<TxTransactionItem> itemList = txTransactionGroup.getItemList();
            if (CollectionUtils.isNotEmpty(itemList)) {
                for (TxTransactionItem item : itemList) {
                    redisTemplate.opsForHash().put(cacheKey(groupId), item.getTaskKey(), item);
                }
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    @Override
    public Boolean saveTxTransactionMsg(String groupId, List<TransactionMsg> transactionMsgs) {
        try {
            //保存数据 到sortSet
            redisTemplate.opsForZSet()
                    .add(CommonConstant.REDIS_KEY_SET, groupId, CommonConstant.REDIS_SCOPE);

            if (CollectionUtils.isNotEmpty(transactionMsgs)) {
                for (TransactionMsg item : transactionMsgs) {
                    redisTemplate.opsForHash().put(cacheKey(groupId), groupId, item);
                }
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    /**
     * 往事务组添加事务
     *
     * @param txGroupId         事务组id
     * @param txTransactionItem 子事务项
     * @return true 成功 false 失败
     */
    @Override
    public Boolean addTxTransaction(String txGroupId, TxTransactionItem txTransactionItem) {
        try {
            redisTemplate.opsForHash().put(cacheKey(txGroupId), txTransactionItem.getTaskKey(), txTransactionItem);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 根据事务组id 获取所有的子项目  我觉得要排除掉第一个，因为第一个不需要进行处理 （它就是事务组信息）
     *
     * @param txGroupId 事务组id
     * @return List<TxTransactionItem>
     */
    @Override
    public List<TxTransactionItem> listByTxGroupId(String txGroupId) {
        final Map<Object, TxTransactionItem> entries = redisTemplate.opsForHash().entries(cacheKey(txGroupId));
        final Collection<TxTransactionItem> values = entries.values();
        return new ArrayList<>(values);


    }

    /**
     * 删除事务组信息  当回滚的时候 或者事务组完全提交的时候
     *
     * @param txGroupId txGroupId 事务组id
     */
    @Override
    public void removeRedisByTxGroupId(String txGroupId) {
        redisTemplate.delete(cacheKey(txGroupId));
    }

    /**
     * 更新事务状态
     *
     * @param key     redis key 也就是txGroupId
     * @param hashKey 也就是taskKey
     * @param status  事务状态
     * @param message 执行结果信息
     * @return true 成功 false 失败
     */
    @Override
    public Boolean updateTxTransactionItemStatus(String key, String hashKey, int status, Object message) {
        try {
            final TxTransactionItem item = (TxTransactionItem)
                    redisTemplate.opsForHash().get(cacheKey(key), hashKey);

            List<TransactionMsg> msgs = (List<TransactionMsg>) item.getArgs()[0];
            msgs.stream().map(msg -> {
                msg.setUpdateTime(System.currentTimeMillis());
                return msg;
            }).forEach(msg -> {
                if (msg != null) {
                    msg.setGroupId(key);
                    msgSource.output().send(MessageBuilder.withPayload(msg).build());
                    redisTemplate.opsForHash().put(cacheKey(key), msg.getSubTaskId(), msg);
                }
            });

            item.setStatus(status);
            if (Objects.nonNull(message)) {
                item.setMessage(message);
            }
            //计算耗时
            final String createDate = item.getCreateDate();

            final LocalDateTime now = LocalDateTime.now();

            try {
                final LocalDateTime createDateTime = DateUtils.parseLocalDateTime(createDate);
                final long consumeTime = DateUtils.getSecondsBetween(createDateTime, now);
                item.setConsumeTime(consumeTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            redisTemplate.opsForHash().put(cacheKey(key), item.getTaskKey(), item);
        } catch (BeansException e) {
            return false;
        }
        return true;
    }

    /**
     * 更新 TM中的消息状态
     *
     * @param key
     * @param hashKey
     * @param status
     */
    @Override
    public Boolean updateTxTransactionMsgStatus(String key, String hashKey, int status) {
        try {
            final TransactionMsg msg = (TransactionMsg) redisTemplate.opsForHash().get(cacheKey(key), hashKey);
            if (msg != null) {
                msg.setConsumed(status);
            }
            redisTemplate.opsForHash().put(cacheKey(key), hashKey, status);
        } catch (Exception e) {
            //TODO 处理异常
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public int findTxTransactionGroupStatus(String txGroupId) {
        try {
            final TxTransactionItem item = (TxTransactionItem)
                    redisTemplate.opsForHash().get(cacheKey(txGroupId), txGroupId);
           /* TxTransactionItem item = new TxTransactionItem();
            BeanUtils.copyProperties(object, item);*/
            return item.getStatus();
        } catch (BeansException e) {
            e.printStackTrace();
            return TransactionStatusEnum.ROLLBACK.getCode();
        }
    }

    @Override
    public Boolean removeCommitTxGroup() {
        final Set<String> keys = redisTemplate.keys(Constant.REDIS_KEYS);
        keys.parallelStream().forEach(key -> {
            final Map<Object, TxTransactionItem> entries = redisTemplate.opsForHash().entries(key);
            final Collection<TxTransactionItem> values = entries.values();
            final boolean present = values.stream()
                    .anyMatch(item -> item.getStatus() != TransactionStatusEnum.COMMIT.getCode());
            if (!present) {
                redisTemplate.delete(key);
            }
        });

        return true;
    }

    /**
     * 删除回滚的事务组
     *
     * @return true 成功  false 失败
     */
    @Override
    public Boolean removeRollBackTxGroup() {
        final Set<String> keys = redisTemplate.keys(Constant.REDIS_KEYS);
        keys.parallelStream().forEach(key -> {
            final Map<Object, TxTransactionItem> entries = redisTemplate.opsForHash().entries(key);
            final Collection<TxTransactionItem> values = entries.values();
            final Optional<TxTransactionItem> any =
                    values.stream().filter(item -> item.getRole() == TransactionRoleEnum.START.getCode()
                            && item.getStatus() == TransactionStatusEnum.ROLLBACK.getCode())
                            .findAny();
            if (any.isPresent()) {
                redisTemplate.delete(key);
            }
        });

        return true;
    }

    private String cacheKey(String key) {
        return String.format(CommonConstant.REDIS_PRE_FIX, key);
    }
}
