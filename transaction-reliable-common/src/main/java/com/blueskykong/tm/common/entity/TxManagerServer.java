
package com.blueskykong.tm.common.entity;

import lombok.Data;

/**
 * @author keets
 */
@Data
public class TxManagerServer {

    private String host;

    private Integer port;


}
